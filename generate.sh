#!/bin/bash

for format in epub pdf docx asciidoc odt; do
    pandoc --toc -o files/for-a-lost-soldier.$format metadata.yml book.md
done

pandoc --standalone --toc -o files/for-a-lost-soldier.html metadata.yml book.md

ebook-convert files/for-a-lost-soldier.epub files/for-a-lost-soldier.mobi